﻿namespace LibTest
{
  public class TestClass
  {
    static public bool TestFunction1(bool input)
    {
      if (input)
      {
        return true;
      }

      return false;
    }

    static public bool TestFunction2(bool input)
    {
      if (input)
      {
        return true;
      }

      return false;
    }
  }
}
