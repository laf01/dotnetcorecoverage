using Xunit;
using LibTest;

namespace TestLibTest
{
  public class UnitTest1
  {
    [Fact]
    public void Test1()
    {
      Assert.False(TestClass.TestFunction1(false));
    }

    [Fact]
    public void Test2()
    {
      Assert.True(TestClass.TestFunction1(true));
    }

    [Fact]
    public void Test3()
    {
      Assert.False(TestClass.TestFunction2(false));
    }

    [Fact]
    public void Test4()
    {
      Assert.True(TestClass.TestFunction2(true));
    }
  }
}
